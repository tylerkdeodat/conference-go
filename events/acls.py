import json

# from urllib import response
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": city + " ," + state + " ," + "US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(geo_url, params=params)
    content = json.loads(response.content)
    latitude = content[0]["lat"]
    longitude = content[0]["lon"]
    # coordinates = str(latitude, longitude)

    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    # headers = {"Authorization": "OPEN_WEATHER_API_KEY"}
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(weather_url, params=params)
    content = json.loads(response.content)
    weather_disc = content["weather"][0]["description"]
    temp = content["main"]["temp"]
    weather_data = {"temperature": temp, "weather_description": weather_disc}
    return weather_data
